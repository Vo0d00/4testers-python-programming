import random
import datetime

# Listy zawierające dane do losowania
# female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
# male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
# surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
# countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Przykład jak można losować imię z listy
# random_female_firstname = random.choice(female_fnames)
# print(random_female_firstname)

# Przykład jak można losować wiek z liczb całkowitych od 1 do 65
# random_age = random.randint(1, 65)
# print(random_age)

# Przykładowy wygenerowany pojedyńczy słownik
#    'firstname': 'Kate',
#   'lastname': 'Yu',
#   'email': 'kate.yu@example.com',
#   'age': 23,
#    'country': 'Poland',
#    'adult': True
# }

#
# Listy zawierające dane do losowania

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Wygenerowanie listy słowników
people_list = []

for i in range(5):
    # Losowanie 5 imion męskich

    firstname = random.choice(male_fnames)
    lastname = random.choice(surnames)
    country = random.choice(countries)
    # Losowanie wieku z przedziału 5,45
    age = random.randint(5, 45)
    if age >= 18:
        adult = True
    # Jeśli wiek 18 lub więcej  adult ustawia się na true
    else:
        adult = False
    # Jeśli wiek mniejszy od 18   adult ustawia się na  false
    # Obliczenie roku urodzenia
    # Jak uzyskać obecny rok https://stackoverflow.com/questions/1133147/how-to-extract-the-year-from-a-python-datetime-object
    current_year = datetime.datetime.now().year
    birth_year = current_year - age
    # Stworzenie słownika
    person = {
        'firstname': firstname,
        'lastname': lastname,
        'email': f"{firstname.lower()}.{lastname.lower()}@example.com",
        'age': age,
        'country': country,
        'adult': adult,
        'birth_year': birth_year,
    }
    # Dodanie słownika do listy
    people_list.append(person)
# Losowanie 5 imion damskich

for i in range(5):

    firstname = random.choice(female_fnames)
    lastname = random.choice(surnames)
    country = random.choice(countries)
    age = random.randint(5, 45)
    if age >= 18:
        adult = True
    else:
        adult = False
    # Jak uzyskać obecny rok https://stackoverflow.com/questions/1133147/how-to-extract-the-year-from-a-python-datetime-object
    current_year = datetime.datetime.now().year
    birth_year = current_year - age

    person = {
        'firstname': firstname,
        'lastname': lastname,
        'email': f"{firstname.lower()}.{lastname.lower()}@example.com",
        'age': age,
        'country': country,
        'adult': adult,
        'birth_year': birth_year,
    }
    people_list.append(person)

which_female_names_drawn = []

for person in people_list:
    if person['firstname'] in female_fnames:
        which_female_names_drawn.append(person['firstname'])

which_male_names_drawn = []
for person in people_list:
    if person['firstname'] in male_fnames:
        which_male_names_drawn.append(person['firstname'])

for person in people_list:
    print(
        f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}.")

if __name__ == '__main__':
    print('Lista słowników :', people_list)
    print(f"W liście znajdują się następujące imiona damskie : {which_female_names_drawn}")
    print(f"W liście znajdują się następujące imiona męskie : {which_male_names_drawn}")
    print('Ilość elementów w liście :', len(people_list))
    print('Ilość imion damskich  wylosowanych :', len(which_female_names_drawn))
    print('Ilość imion męskich  wylosowanych :', len(which_male_names_drawn))
