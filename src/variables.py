first_name = "Paweł"
last_name = "Ojdowski"
age = 37

print(first_name)
print(last_name)
print(age)

# Bellow I describe my friendship details as a set if variables
#nam friend
friends_name = 'Dariusz'
#friend age
friend_age = 38
#number_of_animals
friend_number_of_animals = 4

#has a driving license
friend_has_driving_license = True
#how many years have you known
friendship_duration_in_years =  11.5

print("Friend's name:" ,friends_name, sep='\t')


print(friends_name, friend_age, friend_number_of_animals, friend_has_driving_license , friendship_duration_in_years, sep=',')



