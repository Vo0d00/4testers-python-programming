import random
#import time
import uuid


def generate_random_email():
    domain = 'example.com'
    name_list = ['janek', 'kasia', 'Stefan', 'karolina']
    random_name = random.choice(name_list)
    # suffix = random.randint(1,1_000_000)
    suffix = str(uuid.uuid4())
    return f'{random_name}.{suffix}@{domain}'


def generate_seniority_years():
    return random.randint(0, 40)


def generate_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personal_data():
    random_email = generate_random_email()
    random_number = generate_seniority_years()
    random_boolean = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean
    }


def generate_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_personal_data())
    return list_of_dictionaries


# homework

def get_emails_of_senior_employess(employee_data_list):
    emails = []
    for employee in employee_data_list:
        if employee["seniority_years"] > 10:
            emails.append(employee["email"])
    return emails


def get_emails_of_senior_employes_abovee(employee_data_list, seniority_years_marginn):
    emails = []
    for employee in employee_data_list:
        if employee["seniority_years"] > seniority_years_marginn:
            emails.append(employee["email"])
    return emails


# inne nazwy
def get_emails_of_workers_with_seniority_years_above10(list_of_employees):
    senior_employess = []
    for employee in list_of_employees:
        if employee["seniority_years"] > 10:
            senior_employess.append(employee['email'])
    return senior_employess


def get_emails_of_workers_with_seniority_years_above(list_of_employees, seniority_years_margin):
    senior_employess = []
    for employee in list_of_employees:
        if employee["seniority_years"] > seniority_years_margin:
            senior_employess.append(employee['email'])
    return senior_employess


def get_female_employee_data(employee_data_list):
    female_employee_data = []
    for employee in employee_data_list:
        if employee["female"] == True:
            female_employee_data.append(employee)
    return female_employee_data


def get_list_of_employees_by_genderr(employee_data_list, is_femalee):
    female_employee_data = []
    for employee in employee_data_list:
        if employee["female"] == is_femalee:
            female_employee_data.append(employee)
    return female_employee_data


# inne nazwy
def get_list_of_female_employess(list_of_employees):
    filltered_employees = []
    for employee in list_of_employees:
        if employee["female"] == True:
            filltered_employees.append(employee)
    return filltered_employees


def get_list_of_employees_by_gender(list_of_employees, is_female):
    filltered_employees = []
    for employee in list_of_employees:
        if employee["female"] == is_female:
            filltered_employees.append(employee)
    return filltered_employees


if __name__ == '__main__':
    print(get_dictionary_with_random_personal_data())
    employee_data_list = generate_list_of_dictionaries_with_random_personal_data(20)
    print(employee_data_list)
    senior_employee_emails = get_emails_of_senior_employes_abovee(employee_data_list, 10)
    print(senior_employee_emails)
    female_employee_data = get_female_employee_data(employee_data_list)
    print(female_employee_data)
    test_male_employeee = get_list_of_employees_by_genderr(employee_data_list, False)
    print(test_male_employeee)
    print(len(employee_data_list))
    print(len(test_male_employeee))
    print(len(female_employee_data))


    # konsultacje
    print("piątkowe nazwy i modyfkiacje")
    print(get_dictionary_with_random_personal_data())
    test_employee_list = generate_list_of_dictionaries_with_random_personal_data(30)
    print(test_employee_list)
    test_senior_employee_emails = get_emails_of_workers_with_seniority_years_above(test_employee_list, 10)
    print(test_senior_employee_emails)
    test_female_employess = get_list_of_female_employess(test_employee_list)
    print(test_female_employess)
    test_male_employee =get_list_of_employees_by_gender(test_employee_list , False)
    print(test_male_employee)
    print(len(test_employee_list))
    print(len(test_male_employee))
    print(len(test_female_employess))
