first_name = "Paweł"
last_name = "Ojdowski"
email = "ojdowskipawellukasz@gmail.com"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")
my_bio = "Mam na imię "+ first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imie {first_name}.\nMoje naziwsko to {last_name }.\nMoj email to {email}. "
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}")

####ALgebra ###
circle_radius = 4
area_of_a_circle_with_radius_5 = 3.14 * circle_radius ** 2
circumference_of_a_circle_with_radius_5 = 2* 3.14 *  circle_radius

print("Area of a circle with radius 5:", area_of_a_circle_with_radius_5)
print(f"circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius_5)

long_matematical_expression = 2 +3 +5 *7 +4 / 3 *(3 + 5/ 2) + 7**2 - 13
print(long_matematical_expression)
