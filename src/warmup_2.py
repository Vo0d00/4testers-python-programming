# new1
# Napisz funkcję która zwraca kwadrat dowolnej liczby.
# Policz kwadraty liczb: 0, 16, 2.55
# zamienne nazwy  calculate_2nd_powe_of_number,get calculate jak coś zwraca funkcje
#square kwadrat
def calculate_square_of_random_number(random_number):
    return random_number ** 2


# new2
# Napisz funkcję która konwertuje stopnie Celsjusza na Fahrenheity.
# Ile Fahrenheitów to 20 st. C?
# To convert temperatures in degrees Celsius to Fahrenheit, multiply by 1.8 (or 9/5) and add 32.

def converter_celsius_to_fahrenheit(temperature_celsius):
    return 32 + temperature_celsius * (9 / 5)


def celsius_to_fahrenheit(temp_celsius):
    return 1.8 * temp_celsius + 32


# new3
# Napisz funkcję która zwraca objętość prostopadłościanu o bokach:a, b, c
# Wykorzystując zdefiniowaną funkcję, policz objętość prostopadłościanu o wymiarach 3x5x7
# The volume of cuboid = Base area × Height
# length, breadth, and height długość,szerokość, wysokość
# The base area for cuboid = l × b
# Hence, the volume of a cuboid, V = l × b × h = lbh
# tryb rozkazujący w opisie
# side1,side2,side3 zamiast l,b,h można
def calculate_volume_of_cuboid(l, b, h):
    V = l * b * h
    return V


calculate_volume_of_cuboid(3, 5, 7)

if __name__ == '__main__':
    # zadanie1
    print("Kwadrat  liczby 0  wynosi : ",calculate_square_of_random_number(0))
    print("Kwadrat  liczby 16 wynosi : ",calculate_square_of_random_number(16))
    print("Kwadrat  liczby 2.55  wynosi : ",calculate_square_of_random_number(2.55))

    # zadanie2
    print("20 stopni Celsjusza to : ", converter_celsius_to_fahrenheit(20), ". Pierwszy sposób.")

    # wersja2
    print("20 stopni Celsjusza to : ", celsius_to_fahrenheit(20))

    # zadanie3
    print("Objętość prostopadłościanu wynosi : ", calculate_volume_of_cuboid(3, 5, 7))



