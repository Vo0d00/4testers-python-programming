#zadanie1
#Zdefiniuj zmienną movies zawierającą tytuły 5 Twoich ulubionych filmów

movies = ['Predator Pray', 'Blade runner 2049','HACKERS','Training Day', 'The Pick Of Destiny']
#-1 ostatni element
last_movie = movies[-1]

#append dodawanie
#zmieniamy sam obiekt
movies.append('Eurotrip')
#ctrl + D szybkie kopiowanie linijki
movies.append('Heavy Trip')
#długość listy len
print(len(movies))
# od 2 do 4 wybrać elementy zaczynamy od 2 kończy na elemencie dalej 5
middle_movies = movies[2:5]
print(middle_movies)


movies.insert(0, 'Top Gun 2')
print(movies)



#zadanie2
#Dla listy:
#emails = [‘a@example.com’, ‘b@example.com’]
#Wydrukuj:
#● długość listy
#● pierwszy element
#● ostatni element
#Dodaj nowy email ‘cde@example.com’

emails = ['a@exmample.com','b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')

#zadanie3
#Zdefiniuj zmienną friend zawierającą imię, wiek oraz
# listę dwóch hobby Twojego przyjaciela w postaci słownika.

friend = {


    "name": "Dariusz",
    "age" : "38",
    "hobby" : ["bike" , "electronics"]


}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append("football")
print(friend)
friend["married"]= True
friend["age"] = 44
print(friend)


