import random
import string


# zadanie1
# Napisz funkcję, która przyjmie argument w postaci listy liczb i zwróci ich sumę
# Napisz drugą funkcję, która przyjmie dwie liczby i zwróci ich średnią
# Z użyciem tych funkcji policz średnią temperaturę w ciągu dwóch miesięcy opisanych listami:
# january = [-4, 1.0, -7, 2]
# february= [-13, -9, -3, 3]

# liczenie sredniej dzielimy przez długość len
def calculate_average_of_numbers_in_the_list(input_list):
    return sum(input_list) / len(input_list)


# zwrot średniej
def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


# zadanie 2
# Utwórz funkcję generującą losowe dane do logowania. Argumentem jaki funkcja przyjmuje jest
# email użytkownika (np. “user@example.com”)
# Funkcja powinna zwracać słownik postaci:
# {
# “email”: “user@example.com”,
# “password”: “536a7857-ca1c-4daf-93c3-0b92f5d724ee”
# }
# Gdzie hasło trzeba losowo wygenerować (pomocne:
# https://pynative.com/python-generate-random-string/ )

# Get random password pf length 8 with letters, digits, and symbols
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


# zadanie 3

# Utwórz funkcję, która przyjmie słownik opisujący gracza:
# {
# “nick”: “maestro_54”,
# “type”: “warrior”,
# “exp_points”: 3000
# }
# i drukuje jego opis w postaci:
# “The player maestro_54 is of type warrior and has 3000 EXP”
def calculate_player_level(exp_points):


    return exp_points // 1000


# return int( exp_points / 1000 )


def describe_player(player_dictionary):


    nick = player_dictionary['nick']
    type = player_dictionary['type']
    exp_points = player_dictionary['exp_points']
    level = calculate_player_level(exp_points)

    print(f'The player "{nick}" is of type {type.capitalize()} and has {exp_points} EXP. The player is on level {level}')

if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_numbers_in_the_list(january)
    february_average = calculate_average_of_numbers_in_the_list(february)
    # bimonthly dwumiesiecznie
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

    print(generate_login_data("andor@starwars.com"))
    print(generate_login_data("mando@starwars.com"))
    print(generate_login_data("kenobi@starwars.com"))
    player1 = {

        'nick': 'maestro_54',
        'type': 'warrior',
        'exp_points': 3000,

    }

    player2 = {

    'nick': 'voodoo',
    'type': 'hunter',
    'exp_points': 4000,

    }
    describe_player(player1)
    describe_player(player2)
